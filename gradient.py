import numpy as np
from scipy import stats

import keras
import tensorflow as tf

import pandas as pd
import plotly.express as px  # (version 4.7.0)
import plotly.graph_objects as go

import dash  # (version 1.12.0) pip install dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output
import plotly.graph_objects as go

external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

app = dash.Dash(__name__, external_stylesheets=external_stylesheets)

# ------------------------------------------------------------------------------
# Define functions

def make_normal(min_, max_, size=1000):
    mu = np.mean([min_, max_])
    sigma = (max_ - min_)/10

    normal = np.random.normal(mu, sigma, size)
    return normal

def make_skewed(min_, max_, size=1000, a=10):
    skewed = stats.skewnorm.rvs(10, size=size)
    skewed = (skewed - skewed.min(axis=0)) / (skewed.max(axis=0) - skewed.min(axis=0))
    skewed = skewed * (max_ - min_) + min_
    
    return skewed

def make_bimodal(min_, max_, size=1000):
    m = (max_ - min_)/4
    sigma = (max_ - min_)/20

    mu_left = min_ + m
    mu_right = max_ - m

    left = np.random.normal(mu_left, sigma, int(size/2))
    right = np.random.normal(mu_right, sigma, int(size/2))

    bimodal = np.array([left, right]).flatten()
    return bimodal

def make_uniform(min_, max_, size=1000):
    uniform = np.random.uniform(min_, max_, size)
    return uniform

def make_binary(min_, max_, size=1000):
    binary = np.random.randint(2, size=size)
    map_dict = {0:min_,
                1:max_}
    binary = np.vectorize(map_dict.get)(binary)
    return binary

dict_distributions = {
    'make_normal': make_normal,
    'make_skewed': make_skewed,
    'make_bimodal': make_bimodal,
    'make_uniform': make_uniform,
    'make_binary': make_binary
}

def power_of_ten(integer):
    if integer != 0:
        return (integer/abs(integer))*10**abs(integer)
    else:
        return 1

def make_dataframe(distribution_1, distribution_2, min_1, min_2, max_1, max_2, problem, n_samples=1000):
    df = pd.DataFrame()
    df['x1'] = dict_distributions[distribution_1](min_1, max_2, n_samples)
    df['x2'] = dict_distributions[distribution_2](min_2, max_2, n_samples)

    if problem == 'regression':
        df['y'] = -5*df['x1'] + 10*df['x2']
    else:
        df['y'] = (df['x1'] > df['x2']).astype(int)

    return df

dict_optimizers = {
    'SGD':tf.keras.optimizers.SGD,
    'RMSprop':tf.keras.optimizers.RMSprop,
    'Adagrad':tf.keras.optimizers.Adagrad,
    'Adam':tf.keras.optimizers.Adam
}

def take_weights(data_frame, optimizer, loss, learning_rate):
    model = keras.Sequential()
    model.add(keras.layers.Dense(1, input_dim=2, use_bias=False))
    model.compile(
        optimizer=dict_optimizers[optimizer](learning_rate=learning_rate),
        loss=loss,
        metrics=[loss],
    )

    weight_list = []
    get_weights = keras.callbacks.LambdaCallback(on_train_batch_begin=lambda batch, logs: weight_list.append(model.layers[0].get_weights()[0]))

    model.fit(
        data_frame.drop(columns=['y']),
        data_frame.drop(columns=['x1', 'x2']),
        batch_size=10,
        epochs=1,
        verbose=1,
        validation_split=0.01,
        callbacks=[get_weights]
    )

    return weight_list

def custom_MSE(sc0, sc1, y_, ax=2):
    return ((sc0+sc1 - y_)**2).mean(axis=ax) # Gradient Explodes
    
def custom_MAE(sc0, sc1, y_, ax=2):
    return abs(sc0+sc1 - y_).mean(axis=ax)

def custom_MAPE(sc0, sc1, y_, ax=2):
    return (abs((sc0+sc1 - y_)/y_)).mean(axis=ax)

def custom_BCross(sc0, sc1, y_, ax=2):
    s = sc0 + sc1
    l0 = np.clip(np.log(s), -100, 100)
    l1 = np.clip(np.log(s), -100, 100)
    return -(y_*l0 + (1-y_)*l1).mean(axis=ax) # Gradient Explodes

dict_loss_function = {
    'mean_squared_error': custom_MSE,
    'mean_absolute_error': custom_MAE,
    'mean_absolute_percentage_error': custom_MAPE,
    'binary_crossentropy': custom_BCross
}

def make_contours(weight_list, data_frame, loss, sampling=40, size=1000):
    len_weights = len(weight_list)
    _ = pd.DataFrame(np.array(weight_list).reshape((len_weights, 2)))

    mn = _.min(axis=0)
    mx = _.max(axis=0)

    ax0 = np.linspace(mn[0], mx[0], sampling)
    ax1 = np.linspace(mn[1], mx[1], sampling)

    w0, w1 = np.meshgrid(ax0, ax1)

    sc0 = np.outer(w0.flatten(), data_frame['x1']).reshape(sampling, sampling, size)
    sc1 = np.outer(w1.flatten(), data_frame['x2']).reshape(sampling, sampling, size)

    y_ = np.array([data_frame['y']]*sampling**2).reshape(sampling, sampling, size)

    z = dict_loss_function[loss](sc0, sc1, y_)

    return z, ax0, ax1

def weight_loss_points(weight_list, data_frame, loss):
    len_weights = len(weight_list)
    _ = pd.DataFrame(np.array(weight_list).reshape((len_weights, 2)))
    w1 = _[0]
    w2 = _[1]
    sc0 = np.outer(data_frame['x1'], w1)
    sc1 = np.outer(data_frame['x2'], w2)
    y = np.array([data_frame['y']]*len_weights).T
    z = dict_loss_function[loss](sc0, sc1, y, 0)
    return z

# ------------------------------------------------------------------------------
# App layout
app.layout = html.Div([
    html.Div([
        html.Label(['Feature 1']),
        dcc.RadioItems(
            id='feature_1_distribution',
            options=[
                {'label': 'normal', 'value': 'make_normal'},
                {'label': 'skewed', 'value': 'make_skewed'},
                {'label': 'bimodal', 'value': 'make_bimodal'},
                {'label': 'uniform', 'value': 'make_uniform'},
                {'label': 'binary', 'value': 'make_binary'}
            ],
            value='make_normal',
            style={'columnCount': 5}
        ),
        dcc.RangeSlider(
            id='feature_1_range',
            min=-4,
            max=4,
            step=None,
            marks={
                -4: '-10000',
                -3: '-1000',
                -2: '-100',
                -1: '-10',
                0: '1',
                1: '10',
                2: '100',
                3: '1000',
                4: '10000'
            },
            value=[-2, 2]
        ),
        html.Label(['Feature 2']),
        dcc.RadioItems(
            id='feature_2_distribution',
            options=[
                {'label': 'normal', 'value': 'make_normal'},
                {'label': 'skewed', 'value': 'make_skewed'},
                {'label': 'bimodal', 'value': 'make_bimodal'},
                {'label': 'uniform', 'value': 'make_uniform'},
                {'label': 'binary', 'value': 'make_binary'}
            ],
            value='make_normal',
            style={'columnCount': 5}
        ),
        dcc.RangeSlider(
            id='feature_2_range',
            min=-4,
            max=4,
            step=None,
            marks={
                -4: '-10000',
                -3: '-1000',
                -2: '-100',
                -1: '-10',
                0: '1',
                1: '10',
                2: '100',
                3: '1000',
                4: '10000'
            },
            value=[-2, 2]
        ),
    ],
    style={'columnCount':2}
    ),
    html.Div([
        html.Div([

            html.Label('Type of problem'),
            dcc.RadioItems(
            id='regression_classification',
            options=[
                {'label': 'regression', 'value': 'regression'},
                {'label': 'classification', 'value': 'classification'},
            ],
            value='regression',
            ),

            html.Label('Loss function'),
            dcc.Dropdown(id="loss_function",
                 options=[
                    #  {"label": "MSE", "value": 'mean_squared_error'},
                     {"label": "MAE", "value": 'mean_absolute_error'},
                     {"label": "MAPE", "value": 'mean_absolute_percentage_error'},
                     {"label": "Binary Crossentropy", "value": 'binary_crossentropy'}],
                 multi=False,
                 value='mean_absolute_error',
                 style={'width': "40%"}
                 ),

            html.Label('Optimizer'),
            dcc.Dropdown(id="optimizer",
                 options=[
                     {"label": "SGD", "value": 'SGD'},
                     {'label': "RMSprop", 'value':'RMSprop'},
                     {"label": "Adagrad", "value": 'Adagrad'},
                     {"label": "Adam", "value": 'Adam'}],
                 multi=False,
                 value='SGD',
                 style={'width': "40%"}
                 ),

            html.Label('Learning Rate'),
            dcc.Slider(
                id='learning_rate',
                min=-4,
                max=1,
                step=None,
                marks={
                    -4: '0.0001',
                    -3: '0.001',
                    -2: '0.01',
                    -1: '0.1',
                    0: '1',
                    1: '10'
                },
                value=-2,
            ),

        ],
        style={'padding':30}
        ),

        dcc.Graph(id='gradient_graph', figure={})
    ],
    style={'columnCount':2, 'padding':20}
    )
])


# ------------------------------------------------------------------------------
# Connect the Plotly graphs with Dash Components

@app.callback(
    [Output(component_id='gradient_graph', component_property='figure')],
    [Input(component_id='feature_1_distribution', component_property='value'),
     Input(component_id='feature_1_range', component_property='value'),
     Input(component_id='feature_2_distribution', component_property='value'),
     Input(component_id='feature_2_range', component_property='value'),
     Input(component_id='regression_classification', component_property='value'),
     Input(component_id='loss_function', component_property='value'),
     Input(component_id='optimizer', component_property='value'),
     Input(component_id='learning_rate', component_property='value')]
    )

def update_graph(feature_1_distribution, feature_1_range, feature_2_distribution, feature_2_range, regression_classification, loss_function, optimizer, learning_rate):
    print(feature_1_range)
    print(feature_2_range)

    min_1 = power_of_ten(feature_1_range[0])
    min_2 = power_of_ten(feature_2_range[0])
    max_1 = power_of_ten(feature_1_range[1])
    max_2 = power_of_ten(feature_2_range[1])

    print(f'n1: {min_1}\t x1: {max_1}\t n2: {min_2}\t x2: {max_2}')

    df_ = make_dataframe(feature_1_distribution, feature_2_distribution, min_1, min_2, max_1, max_2, regression_classification)
    weights = take_weights(df_, optimizer, loss_function, learning_rate)
    
    len_weights = len(weights)
    _ = pd.DataFrame(np.array(weights).reshape((len_weights, 2)))
    
    z, x, y = make_contours(weights, df_, loss_function, sampling=40, size=1000)

    z_line = weight_loss_points(weights, df_, loss_function)


    # print(z)
    # print(f'max: {z.max().max()},\tmin: {z.min().min()}')


    # scatter = go.Scatter(x=_[0], y=_[1], mode='lines', line={'color':'red'})
    # countour = go.Contour(z=z, x=x, y=y, colorscale='PuBu')
    # fig = go.Figure(data=[scatter, countour])
    scatter = go.Scatter3d(x=_[0], y=_[1], z=z_line, line={"width":7, "color":'red'}, marker={"color":"red", "size":4})
    surface = go.Surface(z=z, x=x, y=y, colorscale='PuBu')
    fig = go.Figure(data=[scatter, surface])

    return [fig]


if __name__ == '__main__':
    app.run_server(debug=True)